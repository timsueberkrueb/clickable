.. _clickable-json:

clickable.json Format
=====================

Example:

.. code-block:: javascript

    {
        "template": "cmake",
        "scripts": {
            "fetch": "git submodule update --init"
        },
        "dependencies_target": [
            "libpoppler-qt5-dev"
        ]
    }

.. _clickable-json-placeholders:

Placeholders & Environment Variables
------------------------------------

The following placeholders can be used in the clickable.json.
They are also provided as environment variables during build.
When passing ``--debug`` to Clickable, ``DEBUG_BUILD=1`` is set
as an environment variable, additionally.

======================= ======
Placeholder             Output
======================= ======
$ARCH_TRIPLET           Target architecture triplet (``arm-linux-gnueabihf``, ``aarch64-linux-gnu``, ``x86_64-linux-gnu`` or ``all``)
$ROOT                   Value of ``root_dir``
$BUILD_DIR              Value of ``build_dir``
$SRC_DIR                Value of ``src_dir``
$INSTALL_DIR            Value of ``install_dir``
$CLICK_LD_LIBRARY_PATH  ``$INSTALL_DIR/lib/$ARCH_TRIPLET`` or ``$INSTALL_DIR/lib`` for architecture independent apps (will be in ``LD_LIBRARY_PATH`` on runtime)
$CLICK_QML2_IMPORT_PATH ``$INSTALL_DIR/lib/$ARCH_TRIPLET`` or ``$INSTALL_DIR/qml`` for architecture independent apps, which is not in ``QML2_IMPORT_PATH`` at runtime (otherwise will be in ``QML2_IMPORT_PATH`` on runtime)
$CLICK_PATH             ``$INSTALL_DIR/lib/$ARCH_TRIPLET/bin`` or ``$INSTALL_DIR`` for architecture independent apps (will be in ``PATH`` on runtime)
$<lib>_LIB_INSTALL_DIR  Value of ``install_dir`` from ``<lib>`` (see :ref:`libraries <clickable-json-libraries>`)
======================= ======

Parameters accepting placeholders: ``root_dir``, ``build_dir``, ``src_dir``, ``install_dir``, ``gopath``, ``cargo_home``, ``scripts``, ``build``, ``build_args``, ``make_args``, ``postmake``, ``postbuild``, ``prebuild``, ``install_lib``, ``install_bin``, ``install_qml``, ``install_data``. This is an ordered list. Parameters that are used as placeholders themselfs accept only predecessors. Ex: ``$ROOT`` can be used in ``src_dir``, but not vice-versa.

Example:

.. code-block:: javascript

    {
        "template": "cmake",
        "build_dir": "$ROOT/build/$ARCH_TRIPLET/myApp"
    }

clickable_minimum_required
--------------------------

Optional, a minimum Clickable version number required to build the project.
Ex: ``"6"`` or ``"5.4.0"``

.. _clickable-json-arch:

arch
----

Optional, the default is armhf. You may better specify this as a cli arg
(ex: ``--arch arm64``)

.. _clickable-json-template:

template
--------

Optional, see :ref:`build template <build-templates>` for the full list of options.
If left blank the template will be auto detected.


prebuild
--------

Optional, a custom command to run from the root dir, before a build.

build
-----

Optional, a custom command to run from the build dir. This only takes effect if using
the ``custom`` template. It's even required in that case.

postmake
---------

Optional, a custom command to execute from the build directory, after make (during build).

postbuild
---------

Optional, a custom command to execute from the build dir, after build and before click packaging.

build_args
----------

Optional, arguments to pass to qmake or cmake. When using ``--debug``,
``CONFIG+=debug`` is additionally appended for qmake and
``-DCMAKE_BUILD_TYPE=Debug`` for cmake and cordova builds. Ex: ``CONFIG+=ubuntu``

Can be specified as a string or a list of strings.

.. _clickable-json-make-args:

make_args
---------

Optional, arguments to pass to make, e.g. a target name. To avoid configuration
conflicts, the number of make jobs should not be specified here, but using
``make_jobs`` instead, so it can be overriden by the according environment variable.

Can be specified as a string or a list of strings.

.. _clickable-json-make-jobs:

make_jobs
---------

Optional, the number of jobs to use when running make, equivalent to make's ``-j``
option. If left blank this defaults to the number of CPU cores.

launch
------

Optional, a custom command to launch the app, used by ``clickable launch``.

.. _clickable-json-build_dir:

build_dir
---------

Optional, a custom build directory. Defaults to ``$ROOT/build/$ARCH_TRIPLET/app``.
Thanks to the architecture triplet, builds for different architectures can
exist in parallel.

src_dir
-------

Optional, a custom source directory. Defaults to ``$ROOT``

install_dir
-----------

Optional, a custom install directory (used to gather data that goes into the click package). Defaults to ``$BUILD_DIR/install``

.. _clickable-json-install_lib:

install_lib
-----------

Optional, additional libraries that should be installed with the app and be in ``LD_LIBRARY_PATH`` at runtime. The destination directory is ``$CLICK_LD_LIBRARY_PATH``. Ex:

.. code-block:: javascript

    "install_lib": [
        "/usr/lib/$ARCH_TRIPLET/libasound.so*"
    ]

Can be specified as a string or a list of strings.
Supports wildcards as this actually calls ``cp -r <path> $CLICK_LD_LIBRARY_PATH`` in a bash.

install_qml
-----------

Optional, additional QML files or directories that should be installed with the app and be in ``QML2_IMPORT_PATH`` at runtime. The destination directory is ``$CLICK_QML2_IMPORT_PATH``. Ex:

.. code-block:: javascript

    "install_qml": [
        "/usr/lib/$ARCH_TRIPLET/qt5/qml/QtGraphicalEffects"
    ]

Can be specified as a string or a list of strings.
Supports wildcards as this actually calls ``cp -r <path> $CLICK_QML2_IMPORT_PATH`` in a bash.

install_bin
-----------

Optional, additional executables that should be installed with the app and be in ``PATH`` at runtime. The destination directory is ``$CLICK_PATH``. Ex:

.. code-block:: javascript

    "install_bin": [
        "/usr/bin/htop"
    ]

Can be specified as a string or a list of strings.
Supports wildcards as this actually calls ``cp -r <path> $CLICK_PATH`` in a bash.

install_data
------------

Optional, additional files or directories that should be installed with the app. Needs to be specified as a dictionary with absolute source paths as keys and destinations as value. Ex:

.. code-block:: javascript

    "install_data": {
        "$ROOT/packaging/manifest.json": "$INSTALL_DIR",
        "$ROOT/packaging/myapp.desktop": "$INSTALL_DIR"
    },

Supports wildcards as this actually calls ``cp -r <src> <dst>`` in a bash.

kill
----

Optional, a custom process name to kill (used by ``clickable launch`` to kill the app before
relaunching it). If left blank the process name will be assumed.

scripts
-------

Optional, an object detailing custom commands to run. For example:

.. code-block:: javascript

    "scripts": {
        "fetch": "git submodule update --init",
        "echo": "echo $ARCH_TRIPLET"
    }

That enables the use of ``clickable fetch`` and ``clickable echo``.

.. _clickable-json-default:

default
-------

Optional, sub-commands to run when no sub-commands are
specified (running simply ``clickable``). Defaults to ``clean build install launch``.
The ``--dirty`` cli argument removes ``clean`` from that list.

Can be specified as a string or a list of strings.

.. _clickable-json-dirty:

dirty
-----

Optional, whether or not do a dirty build, avoiding to clean the build directory
before building. You may also specify this as a cli arg (``--dirty``).
The default is ``false``.

.. _clickable-json-dependencies_build:

dependencies_build
------------------

Optional, a list of dependencies that will be installed in the build container.

Add dependencies here that are part of your build tool chain.

Can be specified as a string or a list of strings.

.. _clickable-json-dependencies_target:

dependencies_target
-------------------

Optional, a list of dependencies that will be installed in the build container.
These will be assumed to be ``dependency:arch`` (where ``arch`` is the target
architecture), unless an architecture specifier
is already appended.

Add dependencies here that your app actually depends on.

Can be specified as a string or a list of strings.

.. _clickable-json-dependencies-ppa:

dependencies_ppa
----------------

Optional, a list of PPAs, that will be enabled in the build container. Ex:

.. code-block:: javascript

    "dependencies_ppa": [
        "ppa:bhdouglass/clickable"
    ]

Can be specified as a string or a list of strings.

.. _clickable-json-docker-image:

image_setup
-----------
Optional, dictionary containing setup configuration for the docker image used.
The image is based on the default image provided by Clickable.

run
^^^
Optional, a list of commands to run on image setup (each added as `RUN <cmd>` to
the corresponding Dockerfile). Example:

.. code-block:: javascript

    "image_setup": {
        "run": [
            "rustup default nightly",
            "rustup install 1.39.0"
        ]
    }

docker_image
------------

Optional, the name of a docker image to use. When building a custom docker image
it's recommended to use one of the Clickable images as a base. You can find them
on `Docker Hub <https://hub.docker.com/r/clickable/ubuntu-sdk/tags/>`__.

ignore
------

Optional, a list of files to ignore when building a ``pure`` template
Example:

.. code-block:: javascript

    "ignore": [
        ".clickable",
        ".git",
        ".gitignore",
        ".gitmodules"
    ]

Can be specified as a string or a list of strings.

.. _clickable-json-gopath:

gopath
------

Optional, the gopath on the host machine. If left blank, the ``GOPATH`` env var will be used.

.. _clickable-json-cargo_home:

cargo_home
----------

Optional, the Cargo home path on the host machine that is used for caching (namely its subdirs ``registry``, ``git`` and ``.package-cache``).
Defaults to ``~/.clickable/cargo``.

.. _clickable-json-build-args:

root_dir
--------

Optional, specify a different root directory for the project. For example,
if you clickable.json file is in ``platforms/ubuntu_touch`` and you want to include
code from root of your project you can set ``root_dir: "../.."``. Alternatively you can run
clickable from the project root in that case via
``clickable -c platforms/ubuntu_touch/clickable.json``.

.. _clickable-json-test:

test
----

Optional, specify a test command to be executed when running ``clickable test``.
The default is ``qmltestrunner``.

.. _clickable-json-libraries:

libraries
---------
Optional, dependencies to be build by running ``clickable build-libs``.
It's a dictionary of dictionaries similar to the clickable.json itself. Example:

.. code-block:: javascript

    "libraries": {
        "opencv": {
            "template": "cmake",
            "make_jobs": "4",
            "build_args": [
                "-DCMAKE_BUILD_TYPE=Release",
                "-DBUILD_LIST=core,imgproc,highgui,imgcodecs",
                "-DBUILD_SHARED_LIBS=OFF"
            ]
            "prebuild": "git submodule update --init --recursive"
        }
    }

The keywords ``install_dir``, ``prebuild``, ``build``, ``postbuild``,
``postmake``, ``make_jobs``, ``make_args``, ``build_args``, ``docker_image``,
``dependencies_build``, ``dependencies_target`` and ``dependencies_ppa``,
can be used for a library the same way as described above for the app.

In addition to the :ref:`placeholders <clickable-json-placeholders>` described above, the following placeholders are available:

============= ======
Placeholder   Output
============= ======
$NAME         The library name (key name in the ``libraries`` dictionary)
============= ======

A single library can be build by specifying its name as ``clickable build-libs lib1 --arch arm64`` to build the library with name ``lib1`` for the architecture ``arm64``. ``clickable clean-libs lib1 --arch arm64`` cleans the libraries build dir.

template
^^^^^^^^
Required, but only ``cmake``, ``qmake`` and ``custom`` are allowed.

src_dir
^^^^^^^
Optional, library source directory. Must be relative to the project root. Defaults to ``$ROOT/libs/$NAME``

build_dir
^^^^^^^^^
Optional, library build directory. Must be relative to the project root. Defaults to ``$ROOT/build/$ARCH_TRIPLET/$NAME``. Thanks to the architecture triplet, builds for different architectures can
exist in parallel.

Removed keywords
----------------
The following keywords are no longer supported:

- ``dependencies`` (use ``dependencies_target`` and ``dependencies_build`` instead)
- ``specificDependencies``
- ``dir`` (use ``build_dir`` instead)
- ``lxd``
- ``premake`` (use ``prebuild``, ``postmake`` or ``postbuild`` instead)
- ``ssh`` (use program option ``--ssh`` or environment variable ``CLICKABLE_SSH`` instead)
- ``chroot``
- ``sdk``
- ``package``
- ``app``
